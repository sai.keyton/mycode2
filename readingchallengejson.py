#!/usr/bin/env python3
"""Alta3 Research | RZFeeser
   Reading in and writing out JSON"""

# standard library
import json

def main():
    with open("challengeprice.json", "r") as myfile:
        myjson = json.load(myfile)

    with open("challengeprice.text", "w") as myfile:
        myfile.write(str(myjson["assest"]) + " " + myjson["value"] + " " + myjson["description"])

if __name__ == "__main__":
    main()

